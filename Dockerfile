FROM debian:bookworm
RUN apt-get -qy update
RUN apt-get -qy upgrade
RUN apt-get -qy install \
	git \
	python3-pip \
	python3-venv \
	python-is-python3 \
	rsync
RUN apt-get clean
